package ir.vbile.app.rahakish.feature.product

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.snackbar.Snackbar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ir.vbile.app.rahakish.R
import ir.vbile.app.rahakish.common.EXTRA_KEY_ID
import ir.vbile.app.rahakish.common.RahaActivity
import ir.vbile.app.rahakish.common.RahaKishCompletableObserver
import ir.vbile.app.rahakish.common.formatPrice
import ir.vbile.app.rahakish.data.Comment
import ir.vbile.app.rahakish.feature.ProductDetailVM
import ir.vbile.app.rahakish.feature.product.comment.CommentListActivity
import ir.vbile.app.rahakish.services.ImageLoadingService
import ir.vbile.app.rahakish.view.scroll.ObservableScrollViewCallbacks
import ir.vbile.app.rahakish.view.scroll.ScrollState
import kotlinx.android.synthetic.main.activity_product_detail.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import timber.log.Timber

class ProductDetailActivity : RahaActivity() {
    private val commentAdapter = CommentAdapter()
    val vm: ProductDetailVM by viewModel { parametersOf(intent.extras) }
    val compositeDisposable = CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        val imageLoadingService: ImageLoadingService by inject()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)
        initViews()
        vm.product.observe(this) {
            Timber.i(it.toString())
            imageLoadingService.load(productIv, it.image)
            previousPriceTv.text = formatPrice(it.previous_price)
            previousPriceTv.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            currentPriceTv.text = formatPrice(it.price)
            titleTv.text = it.title
            toolbarTitleTv.text = it.title
        }
        vm.comment.observe(this) {
            if (it.size > 3) {
                btnViewAllComments.visibility = View.VISIBLE
                btnViewAllComments.setOnClickListener {
                    startActivity(
                        Intent(
                            this, CommentListActivity::class.java
                        ).apply {
                            putExtra(EXTRA_KEY_ID, vm.product.value!!.id)
                        }
                    )
                }
            } else {
                btnViewAllComments.visibility = View.GONE
            }
            commentAdapter.comments = it as ArrayList<Comment>
        }
        vm.progressBarLiveData.observe(this) {
            setProgressIndicator(it)
        }
    }

    fun initViews() {
        rvComments.apply {
            adapter = commentAdapter
        }
        backBtn.setOnClickListener {
            onBackPressed()
        }
        btnAddToCart.setOnClickListener {
            vm.onAddToCartClicked()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : RahaKishCompletableObserver(compositeDisposable) {
                    override fun onComplete() {
                        showSnackBar(getString(R.string.success_addToCart))
                    }
                })
        }
        productIv.post {
            val productIvHeight = productIv.measuredHeight
            val toolbar = toolbarView
            val productImageView = productIv
            observableScrollView.addScrollViewCallbacks(object : ObservableScrollViewCallbacks {
                override fun onScrollChanged(
                    scrollY: Int,
                    firstScroll: Boolean,
                    dragging: Boolean
                ) {
                    toolbar.alpha = scrollY.toFloat() / productIvHeight
                    productImageView.translationY = scrollY.toFloat() / 2
                }

                override fun onDownMotionEvent() = Unit
                override fun onUpOrCancelMotionEvent(scrollState: ScrollState?) = Unit
            })
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}