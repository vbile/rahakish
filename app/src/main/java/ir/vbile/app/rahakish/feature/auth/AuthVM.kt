package ir.vbile.app.rahakish.feature.auth

import io.reactivex.Completable
import ir.vbile.app.rahakish.common.RahaViewModel
import ir.vbile.app.rahakish.data.repo.UserRepository

class AuthVM constructor(
    private val userRepository: UserRepository
) : RahaViewModel() {
    fun login(email: String, password: String): Completable {
        progressBarLiveData.value = true
        return userRepository.login(email, password).doFinally {
            progressBarLiveData.postValue(false)
        }
    }

    fun signUp(email: String, password: String): Completable {
        progressBarLiveData.value = true
        return userRepository.sigUp(email, password).doFinally {
            progressBarLiveData.postValue(false)
        }
    }
}