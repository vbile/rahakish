package ir.vbile.app.rahakish.data

import androidx.annotation.StringRes

data class EmptyState(
    val mustShow: Boolean,
    @StringRes val messageResourceId: Int = 0,
    val mustShowCallToActionButton: Boolean = false
)
