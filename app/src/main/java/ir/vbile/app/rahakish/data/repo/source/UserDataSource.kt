package ir.vbile.app.rahakish.data.repo.source

import io.reactivex.Completable
import io.reactivex.Single
import ir.vbile.app.rahakish.data.MessageResponse
import ir.vbile.app.rahakish.data.TokenResponse

interface UserDataSource {
    fun login(username: String, password: String): Single<TokenResponse>
    fun sigUp(username: String, password: String): Single<MessageResponse>
    fun loadToken()
    fun saveToken(token: String?, refreshToken: String?)
}