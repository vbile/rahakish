package ir.vbile.app.rahakish.services

import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.view.SimpleDraweeView
import ir.vbile.app.rahakish.view.RahaImageView

class FrescoImageLoadingServiceImpl : ImageLoadingService {
    override fun load(imageView: RahaImageView, imageUrl: String) {
        if (imageView is SimpleDraweeView)
            imageView.setImageURI(imageUrl)
        else
            throw IllegalStateException("ImageView must be instance of SimpleDraweeImageView")
    }
}