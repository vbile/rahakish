package ir.vbile.app.rahakish.feature.cart

import android.content.Intent
import android.os.Bundle
import android.view.View
import ir.vbile.app.rahakish.R
import ir.vbile.app.rahakish.common.EXTRA_KEY_DATA
import ir.vbile.app.rahakish.common.RahaFragment
import ir.vbile.app.rahakish.common.RahaKishCompletableObserver
import ir.vbile.app.rahakish.common.asyncNetworkRequest
import ir.vbile.app.rahakish.data.CartItem
import ir.vbile.app.rahakish.feature.auth.AuthActivity
import ir.vbile.app.rahakish.feature.product.ProductDetailActivity
import ir.vbile.app.rahakish.services.ImageLoadingService
import kotlinx.android.synthetic.main.fragment_cart.*
import kotlinx.android.synthetic.main.view_empty_state.*
import kotlinx.android.synthetic.main.view_empty_state.view.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class CartFragment : RahaFragment(R.layout.fragment_cart), CartItemAdapter.CartItemCallbacks {
    val vm: CartVM by viewModel()
    val imageLoadingService: ImageLoadingService by inject()
    private var cartItemAdapter: CartItemAdapter? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm.progressBarLiveData.observe(viewLifecycleOwner) {
            setProgressIndicator(it)
        }
        vm.cartItems.observe(viewLifecycleOwner) {
            cartItemAdapter =
                CartItemAdapter(it as MutableList<CartItem>, imageLoadingService, this)
            rvItems.adapter = cartItemAdapter
        }
        vm.purchaseDetail.observe(viewLifecycleOwner) { purchaseDetail ->
            cartItemAdapter?.let { adapter ->
                adapter.purchaseDetail = purchaseDetail
                adapter.notifyItemChanged(adapter.cartItems.size)
            }
        }
        vm.emptyState.observe(viewLifecycleOwner) {
            if (it.mustShow) {
                val emptyState = showEmptyState(R.layout.view_empty_state)
                emptyState?.let { view ->
                    view.tvEmptyStateMessage.text = getString(it.messageResourceId)
                    view.btnEmptyStateCta.visibility =
                        if (it.mustShowCallToActionButton) View.VISIBLE else View.GONE
                    view.btnEmptyStateCta.setOnClickListener {
                        startActivity(Intent(context, AuthActivity::class.java))
                    }
                }
            } else {
                viewEmptyState?.visibility = View.GONE
            }
        }
    }

    override fun onStart() {
        super.onStart()
        vm.refresh()
    }

    override fun onRemoveCartItemButtonClick(cartItem: CartItem) {
        vm.removeItemFromCart(cartItem)
            .asyncNetworkRequest()
            .subscribe(object : RahaKishCompletableObserver(compositeDisposable) {
                override fun onComplete() {
                    cartItemAdapter?.removeCartItem(cartItem)
                }
            })
    }

    override fun onIncreaseCartItemButtonClick(cartItem: CartItem) {
        vm.increaseCartItemCount(cartItem)
            .asyncNetworkRequest()
            .subscribe(object : RahaKishCompletableObserver(compositeDisposable) {
                override fun onComplete() {
                    cartItemAdapter?.increaseCartItem(cartItem)
                }
            })
    }

    override fun onDecreaseCartItemButtonClick(cartItem: CartItem) {
        vm.decreaseCartItemCount(cartItem)
            .asyncNetworkRequest()
            .subscribe(object : RahaKishCompletableObserver(compositeDisposable) {
                override fun onComplete() {
                    cartItemAdapter?.decreaseCartItem(cartItem)
                }
            })
    }

    override fun onProductImageClick(cartItem: CartItem) {
        startActivity(Intent(requireContext(), ProductDetailActivity::class.java).apply {
            putExtra(EXTRA_KEY_DATA, cartItem.cartItemId)
        })
    }
}