package ir.vbile.app.rahakish.feature

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.Completable
import ir.vbile.app.rahakish.common.EXTRA_KEY_DATA
import ir.vbile.app.rahakish.common.RahaKishSingleObserver
import ir.vbile.app.rahakish.common.RahaViewModel
import ir.vbile.app.rahakish.common.asyncNetworkRequest
import ir.vbile.app.rahakish.data.Comment
import ir.vbile.app.rahakish.data.Product
import ir.vbile.app.rahakish.data.repo.CartRepository
import ir.vbile.app.rahakish.data.repo.CommentRepository

class ProductDetailVM constructor(
    bundle: Bundle,
    private val commentRepository: CommentRepository,
    private val cartRepository: CartRepository
) : RahaViewModel() {
    private val _product: MutableLiveData<Product> = MutableLiveData()
    val product: LiveData<Product> = _product
    private val _comment: MutableLiveData<List<Comment>> = MutableLiveData()
    val comment: LiveData<List<Comment>> = _comment

    init {
        _product.value = bundle.getParcelable(EXTRA_KEY_DATA)
        progressBarLiveData.value = true
        commentRepository.getAll(_product.value!!.id)
            .asyncNetworkRequest()
            .doFinally {
                progressBarLiveData.value = false
            }
            .subscribe(object : RahaKishSingleObserver<List<Comment>>(compositeDisposable) {
                override fun onSuccess(list: List<Comment>) {
                    _comment.postValue(list)
                }
            })
    }

    fun onAddToCartClicked(): Completable = cartRepository.addToCart(product.value!!.id).ignoreElement()
}