package ir.vbile.app.rahakish.data.repo.source

import android.content.SharedPreferences
import io.reactivex.Single
import ir.vbile.app.rahakish.data.MessageResponse
import ir.vbile.app.rahakish.data.TokenContainer
import ir.vbile.app.rahakish.data.TokenResponse
import timber.log.Timber

class UserLocalDataSource constructor(
    private val sharedPreferences: SharedPreferences
) : UserDataSource {
    override fun login(username: String, password: String): Single<TokenResponse> {
        TODO("Not yet implemented")
    }

    override fun sigUp(username: String, password: String): Single<MessageResponse> {
        TODO("Not yet implemented")
    }

    override fun loadToken() {
        TokenContainer.update(
            sharedPreferences.getString("access_token", null),
            sharedPreferences.getString("refresh_token", null)
        )
        Timber.i("")
    }

    override fun saveToken(token: String?, refreshToken: String?) {
        sharedPreferences.edit().apply {
            putString("access_token", token)
            putString("refresh_token", refreshToken)
        }.apply()
        Timber.i("")
    }
}