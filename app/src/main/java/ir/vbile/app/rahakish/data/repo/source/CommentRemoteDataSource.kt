package ir.vbile.app.rahakish.data.repo.source

import io.reactivex.Single
import ir.vbile.app.rahakish.data.Banner
import ir.vbile.app.rahakish.data.Comment
import ir.vbile.app.rahakish.services.http.ApiService

class CommentRemoteDataSource constructor(
    private val apiService: ApiService
) : CommentDataSource {
    override fun getAll(productId: Int): Single<List<Comment>> =
        apiService.getComments(productId)

    override fun insert(): Single<Comment> {
        TODO("Not yet implemented")
    }
}