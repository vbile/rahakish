package ir.vbile.app.rahakish.feature.auth

import android.os.Bundle
import ir.vbile.app.rahakish.R
import ir.vbile.app.rahakish.common.RahaActivity

class AuthActivity : RahaActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainer, LoginFragment())
        }.commitNow()
    }
}