package ir.vbile.app.rahakish.feature.auth

import android.os.Bundle
import android.view.View
import ir.vbile.app.rahakish.R
import ir.vbile.app.rahakish.common.RahaFragment
import ir.vbile.app.rahakish.common.RahaKishCompletableObserver
import ir.vbile.app.rahakish.common.asyncNetworkRequest
import kotlinx.android.synthetic.main.fragment_login.*
import org.koin.android.viewmodel.ext.android.viewModel

class LoginFragment : RahaFragment(R.layout.fragment_login) {
    private val vm: AuthVM by viewModel()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnLogin.setOnClickListener {
            vm.login(etEmail.text.toString(), etPassword.text.toString())
                .asyncNetworkRequest()
                .subscribe(object : RahaKishCompletableObserver(compositeDisposable) {
                    override fun onComplete() {
                        requireActivity().finish()
                    }
                })
        }
        btnSignUp.setOnClickListener {
            requireActivity().supportFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainer, SignUpFragment())
            }.commit()
        }
    }
}