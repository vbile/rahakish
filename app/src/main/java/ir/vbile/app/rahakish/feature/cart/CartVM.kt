package ir.vbile.app.rahakish.feature.cart

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import io.reactivex.Completable
import ir.vbile.app.rahakish.R
import ir.vbile.app.rahakish.common.RahaKishSingleObserver
import ir.vbile.app.rahakish.common.RahaViewModel
import ir.vbile.app.rahakish.common.asyncNetworkRequest
import ir.vbile.app.rahakish.data.*
import ir.vbile.app.rahakish.data.repo.CartRepository
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import timber.log.Timber

class CartVM constructor(
    private val cartRepository: CartRepository
) : RahaViewModel() {
    private val _cartItems: MutableLiveData<List<CartItem>> = MutableLiveData()
    val cartItems: LiveData<List<CartItem>> = _cartItems

    private val _purchaseDetail: MutableLiveData<PurchaseDetail> = MutableLiveData()
    val purchaseDetail: LiveData<PurchaseDetail> = _purchaseDetail

    init {
        viewModelScope.launch {
            cartRepository.get()
                .asyncNetworkRequest()
                .subscribe { result ->

                }
        }
    }

    private fun getCartItems() {
        if (!TokenContainer.token.isNullOrEmpty()) {
            progressBarLiveData.postValue(true)
            _emptyState.postValue(EmptyState(false))
            cartRepository.get()
                .asyncNetworkRequest()
                .subscribe(object : RahaKishSingleObserver<CartResponse>(compositeDisposable) {
                    override fun onSuccess(response: CartResponse) {
                        if (response.cartItems.isNotEmpty()) {
                            _cartItems.postValue(response.cartItems)
                            _purchaseDetail.postValue(
                                PurchaseDetail(
                                    response.totalPrice,
                                    response.shippingCost,
                                    response.payablePrice
                                )
                            )
                            progressBarLiveData.value = false
                        } else {
                            _emptyState.postValue(EmptyState(true, R.string.cartEmptyState))
                        }
                    }
                })
        } else {
            _emptyState.postValue(EmptyState(true, R.string.cartEmptyStateLoginRequired))
        }
    }

    fun removeItemFromCart(cartItem: CartItem): Completable =
        cartRepository.remove(cartItem.cartItemId)
            .doAfterSuccess {
                Timber.i("Cart Items Count After Remove -> ${cartItems.value?.size}")
                calculateAndPublishPurchaseDetail()
                val cartItemCount = EventBus.getDefault().getStickyEvent(CartItemCount::class.java)
                cartItemCount?.let {
                    it.count -= cartItem.count
                    EventBus.getDefault().postSticky(it)
                }
                cartItems.value?.let {
                    if (it.isEmpty()) {
                        _emptyState.postValue(
                            EmptyState(
                                true,
                                R.string.cartEmptyState
                            )
                        )
                    }
                }
            }.ignoreElement()

    fun increaseCartItemCount(cartItem: CartItem): Completable =
        cartRepository.changeCount(cartItem.cartItemId, ++cartItem.count)
            .doAfterSuccess {
                calculateAndPublishPurchaseDetail()
                val cartItemCount = EventBus.getDefault().getStickyEvent(CartItemCount::class.java)
                cartItemCount?.let {
                    it.count++
                    EventBus.getDefault().postSticky(it)
                }
            }.ignoreElement()

    fun decreaseCartItemCount(cartItem: CartItem): Completable =
        cartRepository.changeCount(cartItem.cartItemId, --cartItem.count)
            .doAfterSuccess {
                calculateAndPublishPurchaseDetail()
                val cartItemCount = EventBus.getDefault().getStickyEvent(CartItemCount::class.java)
                cartItemCount?.let {
                    it.count--
                    EventBus.getDefault().postSticky(it)
                }
            }.ignoreElement()

    fun refresh() {
        getCartItems()
    }

    private fun calculateAndPublishPurchaseDetail() {
        cartItems.value?.let { cartItems ->
            _purchaseDetail.value?.let { purchaseDetail ->
                var totalPrice = 0
                var payablePrice = 0
                cartItems.forEach {
                    totalPrice += it.product.price * it.count
                    payablePrice += (it.product.price - it.product.discount) * it.count
                }
                purchaseDetail.totalPrice = totalPrice
                purchaseDetail.payablePrice = payablePrice
                _purchaseDetail.postValue(purchaseDetail)
            }
        }
    }
}