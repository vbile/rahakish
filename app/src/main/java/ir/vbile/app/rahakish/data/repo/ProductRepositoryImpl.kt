package ir.vbile.app.rahakish.data.repo

import io.reactivex.Completable
import io.reactivex.Single
import ir.vbile.app.rahakish.data.Product
import ir.vbile.app.rahakish.data.SORT_LATEST
import ir.vbile.app.rahakish.data.repo.source.ProductDataSource

class ProductRepositoryImpl(
    val remoteProductDataSource: ProductDataSource,
    val localProductDataSource: ProductDataSource
) : ProductRepository {
    override fun getAll(sort: Int): Single<List<Product>> =
        remoteProductDataSource.getAll(sort)

    override fun getFavoriteProducts(): Single<List<Product>> {
        TODO("Not yet implemented")
    }

    override fun addToFavoriteProducts(): Completable {
        TODO("Not yet implemented")
    }

    override fun deleteFromFavoriteProducts(): Completable {
        TODO("Not yet implemented")
    }
}