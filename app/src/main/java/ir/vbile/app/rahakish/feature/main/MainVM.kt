package ir.vbile.app.rahakish.feature.main

import io.reactivex.schedulers.Schedulers
import ir.vbile.app.rahakish.common.RahaKishSingleObserver
import ir.vbile.app.rahakish.common.RahaViewModel
import ir.vbile.app.rahakish.data.CartItemCount
import ir.vbile.app.rahakish.data.TokenContainer
import ir.vbile.app.rahakish.data.repo.CartRepository
import org.greenrobot.eventbus.EventBus

class MainVM constructor(
    private val cartRepository: CartRepository
) : RahaViewModel() {
    fun getCartItemsCount() {
        if (!TokenContainer.token.isNullOrEmpty()) {
            cartRepository.getCartItemsCount()
                .subscribeOn(Schedulers.io())
                .subscribe(object : RahaKishSingleObserver<CartItemCount>(compositeDisposable) {
                    override fun onSuccess(cartItemCount: CartItemCount) {
                        EventBus.getDefault().postSticky(cartItemCount)
                    }
                })
        }
    }
}