package ir.vbile.app.rahakish.services

import ir.vbile.app.rahakish.view.RahaImageView

interface ImageLoadingService {
    fun load(imageView: RahaImageView, imageUrl: String)
}