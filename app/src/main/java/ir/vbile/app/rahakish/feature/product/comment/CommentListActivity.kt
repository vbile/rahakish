package ir.vbile.app.rahakish.feature.product.comment

import android.os.Bundle
import android.view.View
import ir.vbile.app.rahakish.R
import ir.vbile.app.rahakish.common.EXTRA_KEY_ID
import ir.vbile.app.rahakish.common.RahaActivity
import ir.vbile.app.rahakish.data.Comment
import ir.vbile.app.rahakish.feature.product.CommentAdapter
import kotlinx.android.synthetic.main.activity_comment_list.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class CommentListActivity : RahaActivity() {
    val vm: CommentListVM by viewModel { parametersOf(intent.extras!!.getInt(EXTRA_KEY_ID)) }
    private val commentAdapter = CommentAdapter(true)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comment_list)
        toolbarCommentList.onBackBtnClickListener = View.OnClickListener {
            onBackPressed()
        }
        vm.comment.observe(this) {
            commentAdapter.comments = it as ArrayList<Comment>
        }
        rvComments.apply {
            adapter = commentAdapter
        }
        vm.progressBarLiveData.observe(this) {
            setProgressIndicator(it)
        }
    }
}