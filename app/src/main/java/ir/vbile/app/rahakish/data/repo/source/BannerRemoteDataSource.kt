package ir.vbile.app.rahakish.data.repo.source

import io.reactivex.Single
import ir.vbile.app.rahakish.data.Banner
import ir.vbile.app.rahakish.services.http.ApiService

class BannerRemoteDataSource constructor(
    private val apiService: ApiService
) : BannerDataSource {
    override fun getAll(): Single<List<Banner>> =
        apiService.getBanners()
}