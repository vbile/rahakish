package ir.vbile.app.rahakish.feature.home

import android.content.Intent
import android.os.Bundle
import android.view.View
import ir.vbile.app.rahakish.R
import ir.vbile.app.rahakish.common.EXTRA_KEY_DATA
import ir.vbile.app.rahakish.common.RahaFragment
import ir.vbile.app.rahakish.common.convertDpToPixel
import ir.vbile.app.rahakish.data.Product
import ir.vbile.app.rahakish.data.SORT_LATEST
import ir.vbile.app.rahakish.feature.common.ProductListAdapter
import ir.vbile.app.rahakish.feature.common.VIEW_TYPE_ROUND
import ir.vbile.app.rahakish.feature.list.ProductListActivity
import ir.vbile.app.rahakish.feature.main.BannerSliderAdapter
import ir.vbile.app.rahakish.feature.product.ProductDetailActivity
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class HomeFragment : RahaFragment(R.layout.fragment_home),
    ProductListAdapter.OnProductClickListener {
    val vm: HomeVM by viewModel()
    val productListAdapter: ProductListAdapter by inject{ parametersOf(VIEW_TYPE_ROUND)}
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        productListAdapter.productClickListener = this
        btnViewLatestProduct.setOnClickListener {
            startActivity(
                Intent(requireContext(), ProductListActivity::class.java).apply {
                    putExtra(EXTRA_KEY_DATA, SORT_LATEST)
                }
            )
        }
        subscribeToObservers()
    }

    private fun subscribeToObservers() {
        vm.progressBarLiveData.observe(viewLifecycleOwner) {
            setProgressIndicator(it)
        }
        vm.products.observe(viewLifecycleOwner) {
            productListAdapter.produtcs = it as ArrayList<Product>
        }
        vm.banners.observe(viewLifecycleOwner) {
            val bannerSliderAdapter = BannerSliderAdapter(this, it)
            bannerSlider.adapter = bannerSliderAdapter
            val bannerSliderHeight =
                (((bannerSlider.width - convertDpToPixel(
                    32F,
                    requireContext()
                )) * 173) / 328).toInt()
            val lp = bannerSlider.layoutParams
            lp.height = bannerSliderHeight
            bannerSlider.layoutParams = lp
            sliderIndicator.setViewPager2(bannerSlider)
            latestProducts.apply {
                adapter = productListAdapter
            }
        }
    }

    override fun onProductClick(product: Product) {
        startActivity(Intent(requireContext(), ProductDetailActivity::class.java).apply {
            putExtra(EXTRA_KEY_DATA, product)
        })
    }
}