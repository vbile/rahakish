package ir.vbile.app.rahakish.data.repo

import io.reactivex.Completable
import io.reactivex.Single
import ir.vbile.app.rahakish.data.Product

interface ProductRepository {
    fun getAll(sort: Int): Single<List<Product>>
    fun getFavoriteProducts(): Single<List<Product>>
    fun addToFavoriteProducts(): Completable
    fun deleteFromFavoriteProducts(): Completable
}