package ir.vbile.app.rahakish.feature.product.comment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ir.vbile.app.rahakish.common.RahaKishSingleObserver
import ir.vbile.app.rahakish.common.RahaViewModel
import ir.vbile.app.rahakish.common.asyncNetworkRequest
import ir.vbile.app.rahakish.data.Comment
import ir.vbile.app.rahakish.data.repo.CommentRepository

class CommentListVM constructor(
    productId: Int,
    private val commentRepository: CommentRepository
) : RahaViewModel() {
    private val _comment: MutableLiveData<List<Comment>> = MutableLiveData()
    val comment: LiveData<List<Comment>> = _comment

    init {
        progressBarLiveData.value = true
        commentRepository.getAll(productId)
            .asyncNetworkRequest()
            .doFinally {
                progressBarLiveData.value = false
            }
            .subscribe(object : RahaKishSingleObserver<List<Comment>>(compositeDisposable) {
                override fun onSuccess(list: List<Comment>) {
                    _comment.postValue(list)
                }
            })
    }
}