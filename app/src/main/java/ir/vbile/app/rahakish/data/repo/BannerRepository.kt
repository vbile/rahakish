package ir.vbile.app.rahakish.data.repo

import io.reactivex.Single
import ir.vbile.app.rahakish.data.Banner

interface BannerRepository {
    fun getAll() : Single<List<Banner>>
}