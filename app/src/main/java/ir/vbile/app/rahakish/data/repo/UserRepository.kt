package ir.vbile.app.rahakish.data.repo

import io.reactivex.Completable

interface UserRepository {
    fun login(username: String, password: String): Completable
    fun sigUp(username: String, password: String): Completable
    fun loadToken()
}