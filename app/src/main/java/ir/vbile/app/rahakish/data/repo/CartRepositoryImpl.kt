package ir.vbile.app.rahakish.data.repo

import io.reactivex.Single
import ir.vbile.app.rahakish.data.AddToCartResponse
import ir.vbile.app.rahakish.data.CartItemCount
import ir.vbile.app.rahakish.data.CartResponse
import ir.vbile.app.rahakish.data.MessageResponse
import ir.vbile.app.rahakish.data.repo.source.CartDataSource

class CartRepositoryImpl constructor(
    private val cartDateSource: CartDataSource
) : CartRepository {
    override fun addToCart(productId: Int): Single<AddToCartResponse> =
        cartDateSource.addToCart(productId)

    override fun get(): Single<CartResponse> =
        cartDateSource.get()

    override fun remove(cartItemId: Int): Single<MessageResponse> =
        cartDateSource.remove(cartItemId)

    override fun changeCount(cartItemId: Int,count : Int): Single<AddToCartResponse> =
        cartDateSource.changeCount(cartItemId,count)

    override fun getCartItemsCount(): Single<CartItemCount> =
        cartDateSource.getCartItemsCount()
}