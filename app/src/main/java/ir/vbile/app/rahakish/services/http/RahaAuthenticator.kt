package ir.vbile.app.rahakish.services.http

import com.google.gson.JsonObject
import ir.vbile.app.rahakish.data.TokenContainer
import ir.vbile.app.rahakish.data.TokenResponse
import ir.vbile.app.rahakish.data.repo.source.CLIENT_ID
import ir.vbile.app.rahakish.data.repo.source.CLIENT_SECRET
import ir.vbile.app.rahakish.data.repo.source.UserLocalDataSource
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import timber.log.Timber

@KoinApiExtension
class RahaAuthenticator : okhttp3.Authenticator, KoinComponent {
    val apiService: ApiService by inject()
    val userLocalDataSource: UserLocalDataSource by inject()
    override fun authenticate(route: Route?, response: Response): Request? {
        if (TokenContainer.token != null && TokenContainer.refreshToken != null && !response.request.url.pathSegments.last()
                .equals("token", false)
        ) {
            try {
                val token = refreshToken()
                if (token.isEmpty()) {
                    return null
                }
                return response.request.newBuilder().header("Authorization", "Bearer $token")
                    .build()
            } catch (exp: Exception) {
                Timber.e(exp)
            }
        }
        return null
    }

    private fun refreshToken(): String {
        val response: retrofit2.Response<TokenResponse> = apiService.refreshToken(
            JsonObject().apply {
                addProperty("grant_type", "refresh_token")
                addProperty("refresh_token", TokenContainer.refreshToken)
                addProperty("client_id", CLIENT_ID)
                addProperty("client_secret", CLIENT_SECRET)
            }).execute()
        response.body()?.let {
            TokenContainer.update(it.accessToken, it.refreshToken)
            userLocalDataSource.saveToken(it.accessToken, it.refreshToken)
            return it.accessToken ?: ""
        }
        return ""
    }
}