package ir.vbile.app.rahakish.common

import ir.vbile.app.rahakish.R
import org.json.JSONObject
import retrofit2.HttpException
import timber.log.Timber

class RahaKishExceptionMapper {
    companion object {
        fun map(throwable: Throwable): RahaKishException {
            if (throwable is HttpException) {
                try {
                    val errorJsonObject = JSONObject(throwable.response()?.errorBody()!!.string())
                    val errorMessage = errorJsonObject.getString("message")
                    return RahaKishException(
                        if (throwable.code() == 401)
                            RahaKishException.Type.AUTH
                        else
                            RahaKishException.Type.SIMPLE, serverMessage = errorMessage
                    )
                } catch (exp: Exception) {
                    Timber.e(exp)
                }
            }
            return RahaKishException(RahaKishException.Type.SIMPLE, R.string.unknown_error)
        }
    }
}