package ir.vbile.app.rahakish.feature.list

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import ir.vbile.app.rahakish.R
import ir.vbile.app.rahakish.common.EXTRA_KEY_DATA
import ir.vbile.app.rahakish.common.RahaActivity
import ir.vbile.app.rahakish.data.Product
import ir.vbile.app.rahakish.feature.common.ProductListAdapter
import ir.vbile.app.rahakish.feature.common.VIEW_TYPE_LARGE
import ir.vbile.app.rahakish.feature.common.VIEW_TYPE_SMALL
import ir.vbile.app.rahakish.feature.product.ProductDetailActivity
import kotlinx.android.synthetic.main.activity_comment_list.*
import kotlinx.android.synthetic.main.activity_product_list.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import timber.log.Timber

class ProductListActivity : RahaActivity(), ProductListAdapter.OnProductClickListener {
    val vm: ProductListVM by viewModel { parametersOf(intent.extras!!.getInt(EXTRA_KEY_DATA)) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list)
        productListAdapter.productClickListener = this
        val girdLayoutManager = GridLayoutManager(this@ProductListActivity, 2)
        productsRv.run {
            layoutManager = girdLayoutManager
            adapter = productListAdapter
        }
        viewTypeChangerBtn.setOnClickListener {
            if (productListAdapter.viewType == VIEW_TYPE_SMALL) {
                viewTypeChangerBtn.setImageResource(R.drawable.ic_view_type_large)
                productListAdapter.viewType = VIEW_TYPE_LARGE
                girdLayoutManager.spanCount = 1
                productListAdapter.notifyDataSetChanged()
            } else {
                viewTypeChangerBtn.setImageResource(R.drawable.ic_grid)
                productListAdapter.viewType = VIEW_TYPE_SMALL
                girdLayoutManager.spanCount = 2
                productListAdapter.notifyDataSetChanged()
            }
        }
        btnSort.setOnClickListener {
            val dialog = MaterialAlertDialogBuilder(this)
                .setSingleChoiceItems(
                    R.array.sortTitlesArray,
                    vm.sort
                ) { dialogInterface, selectedSortIndex ->
                    vm.onSelectedSortChangeByUseR(selectedSortIndex)
                    dialogInterface.dismiss()
                }
                .setTitle(R.string.sort)
            dialog.show()
        }
        subscribeToObservers()
        toolbarProductList.onBackBtnClickListener = View.OnClickListener {
            onBackPressed()
        }
    }

    val productListAdapter: ProductListAdapter by inject { parametersOf(VIEW_TYPE_SMALL) }
    private fun subscribeToObservers() {
        vm.progressBarLiveData.observe(this) {
            Timber.i(it.toString())
        }
        vm.products.observe(this) {
            productListAdapter.produtcs = it as ArrayList<Product>
        }
        vm.selectedSortTitle.observe(this) {
            tvSelectedSortTitle.text = getString(it)
        }
    }

    override fun onProductClick(product: Product) {
        Timber.i("")
        startActivity(Intent(this, ProductDetailActivity::class.java).apply {
            putExtra(EXTRA_KEY_DATA, product)
        })
    }
}