package ir.vbile.app.rahakish.feature.common

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ir.vbile.app.rahakish.R
import ir.vbile.app.rahakish.common.formatPrice
import ir.vbile.app.rahakish.common.implementSpringAnimationTrait
import ir.vbile.app.rahakish.data.Product
import ir.vbile.app.rahakish.services.ImageLoadingService
import ir.vbile.app.rahakish.view.RahaImageView

const val VIEW_TYPE_ROUND = 0
const val VIEW_TYPE_SMALL = 1
const val VIEW_TYPE_LARGE = 2

class ProductListAdapter constructor(
    var viewType: Int = VIEW_TYPE_ROUND,
    private val imageLoadingService: ImageLoadingService
) :
    RecyclerView.Adapter<ProductListAdapter.ProductViewHolder>() {

    var productClickListener: OnProductClickListener? = null
    var produtcs = ArrayList<Product>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val layoutResId = when (viewType) {
            VIEW_TYPE_ROUND -> R.layout.item_product
            VIEW_TYPE_SMALL -> R.layout.item_product_small
            VIEW_TYPE_LARGE -> R.layout.item_product_large
            else -> throw IllegalStateException("View type is not valid")
        }
        val view = LayoutInflater.from(parent.context).inflate(layoutResId, parent, false)
        return ProductViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int = viewType
    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) =
        holder.bindProduct(produtcs[position])

    override fun getItemCount(): Int = produtcs.size

    inner class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val titleTv = itemView.findViewById<TextView>(R.id.productTitleTv)
        val productIv = itemView.findViewById<RahaImageView>(R.id.productIv)
        val currentPriceTv = itemView.findViewById<TextView>(R.id.currentPriceTv)
        val previousPriceTv = itemView.findViewById<TextView>(R.id.previousPriceTv)
        fun bindProduct(product: Product) {
            imageLoadingService.load(productIv, product.image)
            titleTv.text = product.title
            currentPriceTv.text = formatPrice(product.price)
            previousPriceTv.text = formatPrice(product.previous_price)
            previousPriceTv.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            itemView.implementSpringAnimationTrait()
            itemView.setOnClickListener {
                productClickListener?.onProductClick(product)
            }
        }
    }

    interface OnProductClickListener {
        fun onProductClick(product: Product)
    }
}