package ir.vbile.app.rahakish.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import ir.vbile.app.rahakish.R
import kotlinx.android.synthetic.main.view_toolbar.view.*

class RahaKishToolbar(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {
    var onBackBtnClickListener: OnClickListener? = null
        set(value) {
            field = value
            btnBack.setOnClickListener(onBackBtnClickListener)
        }

    init {
        inflate(context, R.layout.view_toolbar, this)
        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.RahaKishToolbar)
            val title = a.getString(R.styleable.RahaKishToolbar_rk_title)
            if (title != null && title.isNotEmpty())
                tvTitle.text = title
            a.recycle()
        }
    }
}