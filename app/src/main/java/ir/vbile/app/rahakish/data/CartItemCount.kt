package ir.vbile.app.rahakish.data


import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class CartItemCount(
    @SerializedName("count")
    var count: Int
) : Parcelable