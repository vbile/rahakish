package ir.vbile.app.rahakish.data.repo.source

import io.reactivex.Completable
import io.reactivex.Single
import ir.vbile.app.rahakish.data.Product

interface ProductDataSource {
    fun getAll(sort: Int): Single<List<Product>>
    fun getFavoriteProducts(): Single<List<Product>>
    fun addToFavoriteProducts(): Completable
    fun deleteFromFavoriteProducts(): Completable
}