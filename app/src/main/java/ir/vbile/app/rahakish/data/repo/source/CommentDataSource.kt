package ir.vbile.app.rahakish.data.repo.source

import io.reactivex.Single
import ir.vbile.app.rahakish.data.Comment

interface CommentDataSource {
    fun getAll(productId: Int): Single<List<Comment>>
    fun insert(): Single<Comment>
}