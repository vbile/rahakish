package ir.vbile.app.rahakish.feature.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ir.vbile.app.rahakish.common.RahaKishSingleObserver
import ir.vbile.app.rahakish.common.RahaViewModel
import ir.vbile.app.rahakish.common.asyncNetworkRequest
import ir.vbile.app.rahakish.data.Banner
import ir.vbile.app.rahakish.data.Product
import ir.vbile.app.rahakish.data.SORT_LATEST
import ir.vbile.app.rahakish.data.repo.BannerRepository
import ir.vbile.app.rahakish.data.repo.ProductRepository

class HomeVM(
    productRepository: ProductRepository,
    bannerRepository: BannerRepository
) : RahaViewModel() {

    private val _products: MutableLiveData<List<Product>> = MutableLiveData()
    val products: MutableLiveData<List<Product>> = _products


    private val _banners: MutableLiveData<List<Banner>> = MutableLiveData()
    val banners: LiveData<List<Banner>> = _banners

    init {
        progressBarLiveData.value = true
        productRepository.getAll(SORT_LATEST)
            .asyncNetworkRequest()
            .doFinally {
                progressBarLiveData.value = false
            }
            .subscribe(object : RahaKishSingleObserver<List<Product>>(compositeDisposable) {
                override fun onSuccess(products: List<Product>) {
                    _products.postValue(products)
                }
            })

        bannerRepository.getAll()
            .asyncNetworkRequest()
            .doFinally {
                progressBarLiveData.value = false
            }
            .subscribe(object : RahaKishSingleObserver<List<Banner>>(compositeDisposable) {
                override fun onSuccess(banners: List<Banner>) {
                    _banners.postValue(banners)
                }
            })
    }


}