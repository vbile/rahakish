package ir.vbile.app.rahakish.feature.auth

import android.os.Bundle
import android.view.View
import ir.vbile.app.rahakish.R
import ir.vbile.app.rahakish.common.RahaFragment
import ir.vbile.app.rahakish.common.RahaKishCompletableObserver
import ir.vbile.app.rahakish.common.asyncNetworkRequest
import kotlinx.android.synthetic.main.fragment_signup.*
import org.koin.android.viewmodel.ext.android.viewModel

class SignUpFragment : RahaFragment(R.layout.fragment_signup) {
    private val vm: AuthVM by viewModel()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnSignUp.setOnClickListener {
            vm.signUp(etEmail.text.toString(), etPassword.text.toString())
                .asyncNetworkRequest()
                .subscribe(object : RahaKishCompletableObserver(compositeDisposable) {
                    override fun onComplete() {
                        requireActivity().finish()
                    }
                })
        }
        btnLogin.setOnClickListener {
            requireActivity().supportFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainer, LoginFragment())
            }
        }

    }
}