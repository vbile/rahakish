package ir.vbile.app.rahakish.feature.product

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.vbile.app.rahakish.R
import ir.vbile.app.rahakish.data.Comment
import kotlinx.android.synthetic.main.item_comment.view.*

class CommentAdapter constructor(val showAll: Boolean = false) :
    RecyclerView.Adapter<CommentAdapter.CommentViewHolder>() {
    var comments = ArrayList<Comment>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    inner class CommentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindComment(comment: Comment) {
            itemView.apply {
                tvCommentTitle.text = comment.title
                tvCommentDate.text = comment.date
                tvCommentAuthor.text = comment.author.email
                tvCommentContent.text = comment.content
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false)
        return CommentViewHolder(view)
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        holder.bindComment(comments[position])
    }

    override fun getItemCount(): Int = if (comments.size > 3 && !showAll) 3 else comments.size
}