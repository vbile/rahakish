package ir.vbile.app.rahakish.data.repo

import io.reactivex.Single
import ir.vbile.app.rahakish.data.AddToCartResponse
import ir.vbile.app.rahakish.data.CartItemCount
import ir.vbile.app.rahakish.data.CartResponse
import ir.vbile.app.rahakish.data.MessageResponse

interface CartRepository {
    fun addToCart(productId: Int): Single<AddToCartResponse>
    fun get(): Single<CartResponse>
    fun remove(cartItemId: Int): Single<MessageResponse>
    fun changeCount(cartItemId: Int, count: Int): Single<AddToCartResponse>
    fun getCartItemsCount(): Single<CartItemCount>
}