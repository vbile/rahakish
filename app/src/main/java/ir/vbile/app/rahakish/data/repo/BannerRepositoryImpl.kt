package ir.vbile.app.rahakish.data.repo

import io.reactivex.Single
import ir.vbile.app.rahakish.data.Banner
import ir.vbile.app.rahakish.data.repo.source.BannerDataSource

class BannerRepositoryImpl constructor(
    private val remoteBannerDataSource: BannerDataSource
) : BannerRepository {
    override fun getAll(): Single<List<Banner>> =
        remoteBannerDataSource.getAll()
}