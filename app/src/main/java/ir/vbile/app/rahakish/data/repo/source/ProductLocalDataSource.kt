package ir.vbile.app.rahakish.data.repo.source

import io.reactivex.Completable
import io.reactivex.Single
import ir.vbile.app.rahakish.data.Product

class ProductLocalDataSource : ProductDataSource {
    override fun getAll(sort: Int): Single<List<Product>> {
        TODO("Not yet implemented")
    }

    override fun getFavoriteProducts(): Single<List<Product>> {
        TODO("Not yet implemented")
    }

    override fun addToFavoriteProducts(): Completable {
        TODO("Not yet implemented")
    }

    override fun deleteFromFavoriteProducts(): Completable {
        TODO("Not yet implemented")
    }
}