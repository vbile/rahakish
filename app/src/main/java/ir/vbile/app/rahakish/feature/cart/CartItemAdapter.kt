package ir.vbile.app.rahakish.feature.cart

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.vbile.app.rahakish.R
import ir.vbile.app.rahakish.data.CartItem
import ir.vbile.app.rahakish.data.PurchaseDetail
import ir.vbile.app.rahakish.services.ImageLoadingService
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_cart.view.*
import kotlinx.android.synthetic.main.item_purchase_detail.view.*

const val VIEW_TYPE_CART_ITEM = 0
const val VIEW_TYPE_PURCHASE_DETAIL = 1

class CartItemAdapter constructor(
    val cartItems: MutableList<CartItem>,
    private val imageLoadingService: ImageLoadingService,
    private val onCartItemCallbacks: CartItemCallbacks
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var purchaseDetail: PurchaseDetail? = null
    override fun getItemViewType(position: Int): Int {
        return if (position == cartItems.size) {
            VIEW_TYPE_PURCHASE_DETAIL
        } else {
            VIEW_TYPE_CART_ITEM
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_TYPE_CART_ITEM -> CartItemAdapterViewHolder(
                inflater.inflate(
                    R.layout.item_cart,
                    parent,
                    false
                )
            )
            VIEW_TYPE_PURCHASE_DETAIL -> PurchaseDetailViewHolder(
                inflater.inflate(
                    R.layout.item_purchase_detail,
                    parent,
                    false
                )
            )
            else -> TODO()
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is CartItemAdapterViewHolder -> holder.bind(cartItems[position])
            is PurchaseDetailViewHolder -> {
                purchaseDetail?.let {
                    holder.bind(
                        it.totalPrice, it.shippingCost, it.payablePrice
                    )
                }
            }
            else -> Unit
        }
    }


    override fun getItemCount(): Int = cartItems.size + 1
    inner class PurchaseDetailViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(totalPrice: Int, shippingCost: Int, payablePrice: Int) {
            containerView.tvTotalPrice.text = totalPrice.toString()
            containerView.tvShippingCost.text = shippingCost.toString()
            containerView.tvPayablePrice.text = payablePrice.toString()
        }
    }

    inner class CartItemAdapterViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(cartItem: CartItem) {
            containerView.tvProductTitle.text = cartItem.product.title
            containerView.tvCartItemCount.text = cartItem.count.toString()
            containerView.tvPrice.text = cartItem.product.price.toString()
            containerView.tvPreviousPrice.text = cartItem.product.previous_price.toString()
            imageLoadingService.load(containerView.ivProduct, cartItem.product.image)
            containerView.btnRemoveFromCart.setOnClickListener {
                onCartItemCallbacks.onRemoveCartItemButtonClick(cartItem)
            }
            containerView.btnIncrease.setOnClickListener {
                cartItem.changeCountProgressBarIsVisible = true
                containerView.prgChangeCount.visibility = View.VISIBLE
                containerView.tvCartItemCount.visibility = View.INVISIBLE
                onCartItemCallbacks.onIncreaseCartItemButtonClick(cartItem)
            }
            containerView.prgChangeCount.visibility =
                if (cartItem.changeCountProgressBarIsVisible) View.VISIBLE else View.INVISIBLE
            containerView.tvCartItemCount.visibility =
                if (cartItem.changeCountProgressBarIsVisible) View.INVISIBLE else View.VISIBLE

            containerView.btnDecrease.setOnClickListener {
                if (cartItem.count > 1) {
                    cartItem.changeCountProgressBarIsVisible = true
                    containerView.prgChangeCount.visibility = View.VISIBLE
                    containerView.tvCartItemCount.visibility = View.INVISIBLE
                    onCartItemCallbacks.onDecreaseCartItemButtonClick(cartItem)
                }
            }
            if (cartItem.changeCountProgressBarIsVisible) {
                containerView.prgChangeCount.visibility = View.VISIBLE
                containerView.btnIncrease.visibility = View.GONE
            }
            containerView.ivProduct.setOnClickListener {
                onCartItemCallbacks.onProductImageClick(cartItem)
            }
        }
    }

    interface CartItemCallbacks {
        fun onRemoveCartItemButtonClick(cartItem: CartItem)
        fun onIncreaseCartItemButtonClick(cartItem: CartItem)
        fun onDecreaseCartItemButtonClick(cartItem: CartItem)
        fun onProductImageClick(cartItem: CartItem)
    }

    fun removeCartItem(cartItem: CartItem) {
        val index = cartItems.indexOf(cartItem)
        if (index > -1) {
            cartItems.removeAt(index)
            notifyItemRemoved(index)
        }
    }

    fun increaseCartItem(cartItem: CartItem) {
        val index = cartItems.indexOf(cartItem)
        if (index > -1) {
            cartItems[index].changeCountProgressBarIsVisible = false
            notifyItemChanged(index)
        }
    }

    fun decreaseCartItem(cartItem: CartItem) {
        val index = cartItems.indexOf(cartItem)
        if (index > -1) {
            cartItems[index].changeCountProgressBarIsVisible = false
            notifyItemChanged(index)
        }
    }
}