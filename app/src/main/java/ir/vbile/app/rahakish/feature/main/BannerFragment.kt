package ir.vbile.app.rahakish.feature.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import ir.vbile.app.rahakish.R
import ir.vbile.app.rahakish.common.EXTRA_KEY_DATA
import ir.vbile.app.rahakish.data.Banner
import ir.vbile.app.rahakish.services.ImageLoadingService
import ir.vbile.app.rahakish.view.RahaImageView
import org.koin.android.ext.android.inject

class BannerFragment : Fragment() {
    val imageLoadingService: ImageLoadingService by inject()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val imageView = inflater.inflate(
            R.layout.fragment_banner,
            container,
            false
        ) as RahaImageView
        val banner = requireArguments()
            .getParcelable<Banner>(EXTRA_KEY_DATA)
            ?: throw IllegalStateException("Banner cannot be null")
        imageLoadingService.load(imageView, banner.image)
        return imageView
    }

    companion object {
        fun newInstance(banner: Banner): BannerFragment {
            return BannerFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(EXTRA_KEY_DATA, banner)
                }
            }
        }
    }
}