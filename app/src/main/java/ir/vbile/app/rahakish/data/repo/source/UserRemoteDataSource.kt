package ir.vbile.app.rahakish.data.repo.source

import com.google.gson.JsonObject
import io.reactivex.Single
import ir.vbile.app.rahakish.data.MessageResponse
import ir.vbile.app.rahakish.data.TokenResponse
import ir.vbile.app.rahakish.services.http.ApiService

const val CLIENT_ID = 2
const val CLIENT_SECRET = "kyj1c9sVcksqGU4scMX7nLDalkjp2WoqQEf8PKAC"

class UserRemoteDataSource constructor(
    private val apiService: ApiService
) : UserDataSource {
    override fun login(username: String, password: String): Single<TokenResponse> =
        apiService.login(JsonObject().apply {
            addProperty("username", username)
            addProperty("password", password)
            addProperty("grant_type", "password")
            addProperty("client_id", CLIENT_ID)
            addProperty("client_secret", CLIENT_SECRET)
        })
    override fun sigUp(username: String, password: String): Single<MessageResponse> =
        apiService.signUp(JsonObject().apply {
            addProperty("email", username)
            addProperty("password", password)
        })

    override fun loadToken()  {
        TODO("Not yet implemented")
    }


    override fun saveToken(token: String?, refreshToken: String?) {
        TODO("Not yet implemented")
    }

}