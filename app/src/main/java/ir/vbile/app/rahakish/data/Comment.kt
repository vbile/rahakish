package ir.vbile.app.rahakish.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Comment(
    val id: Int,
    val title: String,
    val content: String,
    val date: String,
    val author: Author
) : Parcelable {
    @Parcelize
    data class Author(
        val email: String
    ) : Parcelable
}
