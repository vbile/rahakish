package ir.vbile.app.rahakish.data.repo

import io.reactivex.Single
import ir.vbile.app.rahakish.data.Comment
import ir.vbile.app.rahakish.data.repo.source.CommentDataSource

class CommentRepositoryImpl constructor(
    private val commentDataSource: CommentDataSource
) : CommentRepository {
    override fun getAll(productId: Int): Single<List<Comment>> = commentDataSource.getAll(productId)

    override fun insert(): Single<Comment> =
        commentDataSource.insert()
}