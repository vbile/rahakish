package ir.vbile.app.rahakish.data.repo.source

import io.reactivex.Single
import ir.vbile.app.rahakish.data.Banner

interface BannerDataSource {
    fun getAll() : Single<List<Banner>>
}