package ir.vbile.app.rahakish.feature.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ir.vbile.app.rahakish.R
import ir.vbile.app.rahakish.common.RahaKishSingleObserver
import ir.vbile.app.rahakish.common.RahaViewModel
import ir.vbile.app.rahakish.common.asyncNetworkRequest
import ir.vbile.app.rahakish.data.Product
import ir.vbile.app.rahakish.data.repo.ProductRepository

class ProductListVM constructor(
    var sort: Int,
    private val productRepository: ProductRepository
) : RahaViewModel() {
    private val _products: MutableLiveData<List<Product>> = MutableLiveData()
    val products: LiveData<List<Product>> = _products
    val selectedSortTitle: MutableLiveData<Int> = MutableLiveData()
    val sortTitles = arrayOf(
        R.string.sortLatest,
        R.string.sortPopular,
        R.string.sortPriceHighToLow,
        R.string.sortPriceLowToHigh
    )

    init {
        getProducts(sort)
        selectedSortTitle.value = sortTitles[sort]
    }

    fun getProducts(sort: Int) {
        progressBarLiveData.value = true
        productRepository.getAll(sort)
            .asyncNetworkRequest()
            .doFinally {
                progressBarLiveData.value = false
            }
            .subscribe(object : RahaKishSingleObserver<List<Product>>(compositeDisposable) {
                override fun onSuccess(list: List<Product>) {
                    _products.value = list
                }
            })
    }
    fun onSelectedSortChangeByUseR(sort: Int){
        this.sort = sort
        selectedSortTitle.value = sortTitles[sort]
        getProducts(sort)
    }
}