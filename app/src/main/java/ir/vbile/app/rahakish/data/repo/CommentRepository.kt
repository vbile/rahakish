package ir.vbile.app.rahakish.data.repo

import io.reactivex.Single
import ir.vbile.app.rahakish.data.Comment

interface CommentRepository {
    fun getAll(productId: Int): Single<List<Comment>>
    fun insert(): Single<Comment>
}