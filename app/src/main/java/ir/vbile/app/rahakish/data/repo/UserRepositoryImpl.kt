package ir.vbile.app.rahakish.data.repo

import io.reactivex.Completable
import ir.vbile.app.rahakish.data.TokenContainer
import ir.vbile.app.rahakish.data.TokenResponse
import ir.vbile.app.rahakish.data.repo.source.UserDataSource

class UserRepositoryImpl constructor(
    val userLocalDataSource: UserDataSource,
    val userRemoteDataSource: UserDataSource
) : UserRepository {
    override fun login(username: String, password: String): Completable {
        return userRemoteDataSource.login(username, password).doOnSuccess {
            onSuccessfulLogin(it)
        }.ignoreElement()
    }

    override fun sigUp(username: String, password: String): Completable {
        return userRemoteDataSource.sigUp(username, password).flatMap {
            userRemoteDataSource.login(username, password)
        }.doOnSuccess {
            onSuccessfulLogin(it)
        }.ignoreElement()
    }

    override fun loadToken() {
        userLocalDataSource.loadToken()
    }

    private fun onSuccessfulLogin(tokenResponse: TokenResponse) {
        TokenContainer.update(tokenResponse.accessToken, tokenResponse.refreshToken)
        userLocalDataSource.saveToken(tokenResponse.accessToken, tokenResponse.refreshToken)
    }
}