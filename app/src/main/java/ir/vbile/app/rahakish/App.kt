package ir.vbile.app.rahakish

import android.app.Application
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import com.facebook.drawee.backends.pipeline.Fresco
import ir.vbile.app.rahakish.data.repo.*
import ir.vbile.app.rahakish.data.repo.source.*
import ir.vbile.app.rahakish.feature.ProductDetailVM
import ir.vbile.app.rahakish.feature.auth.AuthVM
import ir.vbile.app.rahakish.feature.cart.CartVM
import ir.vbile.app.rahakish.feature.common.ProductListAdapter
import ir.vbile.app.rahakish.feature.home.HomeVM
import ir.vbile.app.rahakish.feature.list.ProductListVM
import ir.vbile.app.rahakish.feature.main.MainVM
import ir.vbile.app.rahakish.feature.product.comment.CommentListVM
import ir.vbile.app.rahakish.services.FrescoImageLoadingServiceImpl
import ir.vbile.app.rahakish.services.ImageLoadingService
import ir.vbile.app.rahakish.services.http.createApiServiceInstance
import org.koin.android.ext.android.get
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import timber.log.Timber

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        Timber.plant(Timber.DebugTree())
        Fresco.initialize(this)
        val myModules = module {
            single { createApiServiceInstance() }
            single<ImageLoadingService> { FrescoImageLoadingServiceImpl() }
            factory<ProductRepository> {
                ProductRepositoryImpl(
                    ProductRemoteDataSource(get()),
                    ProductLocalDataSource()
                )
            }
            single<SharedPreferences> {
                this@App.getSharedPreferences(
                    "app_settings",
                    MODE_PRIVATE
                )
            }
            single { UserLocalDataSource(get()) }
            single<UserRepository> {
                UserRepositoryImpl(
                    UserLocalDataSource(get()),
                    UserRemoteDataSource(get())
                )
            }
            factory { (viewType: Int) -> ProductListAdapter(viewType, get()) }
            factory<BannerRepository> { BannerRepositoryImpl(BannerRemoteDataSource(get())) }
            factory<CommentRepository> { CommentRepositoryImpl(CommentRemoteDataSource(get())) }
            factory<CartRepository> { CartRepositoryImpl(CartRemoteDateSource(get())) }
            viewModel { HomeVM(get(), get()) }
            viewModel { (bundle: Bundle) -> ProductDetailVM(bundle, get(), get()) }
            viewModel { (productId: Int) -> CommentListVM(productId, get()) }
            viewModel { (sort: Int) -> ProductListVM(sort, get()) }
            viewModel { AuthVM(get()) }
            viewModel { CartVM(get()) }
            viewModel { MainVM(get()) }
        }
        startKoin {
            androidContext(this@App)
            modules(myModules)
        }
        val userRepository: UserRepository = get()
        userRepository.loadToken()
    }
}