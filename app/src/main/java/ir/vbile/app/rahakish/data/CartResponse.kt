package ir.vbile.app.rahakish.data


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CartResponse(
    @SerializedName("cart_items")
    val cartItems: List<CartItem>,
    @SerializedName("payable_price")
    val payablePrice: Int,
    @SerializedName("total_price")
    val totalPrice: Int,
    @SerializedName("shipping_cost")
    val shippingCost: Int
) : Parcelable

@Parcelize
data class CartItem(
    @SerializedName("cart_item_id")
    val cartItemId: Int,
    @SerializedName("product")
    val product: Product,
    @SerializedName("count")
    var count: Int,
    var changeCountProgressBarIsVisible: Boolean = false
) : Parcelable

data class PurchaseDetail(
    var totalPrice: Int,
    var shippingCost: Int,
    var payablePrice: Int
)